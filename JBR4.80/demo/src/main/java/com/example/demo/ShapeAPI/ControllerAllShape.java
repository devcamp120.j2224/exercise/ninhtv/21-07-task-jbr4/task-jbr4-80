package com.example.demo.ShapeAPI;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ControllerAllShape {
    @GetMapping("/circle-area")
    public double getCircleArea(){
        Circle circle = new Circle(3.0);

        return circle.getArea();
    }
    @GetMapping("/circle-perimeter")
    public double getCirclePerimeter(){
        Circle circle = new Circle(3.0);

        return circle.getPerimeter();
    }

    @GetMapping("/rectangle-area")
    public double getRectangleArea(){
        Rectangle rectangle = new Rectangle(5.0 , 30.0);

        return rectangle.getArea();
    }
    @GetMapping("/rectangle-perimeter")
    public double getRectanglePerimeter(){
        Rectangle rectangle = new Rectangle(5.0 , 30.0);

        return rectangle.getPerimeter();
    }
    @GetMapping("/square-area")
    public double getSquareArea(){
        Square square = new Square(9.0);

        return square.getArea();
    }
    @GetMapping("/square-perimeter")
    public double getSquarePerineter(){
        Square square = new Square(7.0);

        return square.getPerimeter();
    }
}
